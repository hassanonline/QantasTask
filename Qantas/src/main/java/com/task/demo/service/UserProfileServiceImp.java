package com.task.demo.service;

import com.task.demo.model.UserProfile;
import com.task.demo.repository.IUserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by hassanadeel on 18/6/18.
 */
@Service
public class UserProfileServiceImp implements IUserProfileService {

    @Autowired
    private IUserProfileRepository userProfileRepository;

    @Override
    public UserProfile createUserProfile(final UserProfile userProfile) {
        userProfileRepository.save(userProfile);
        return userProfile;
    }

    @Override
    public ResponseEntity updateUserProfile(final UserProfile userProfile) {
        Optional<UserProfile> userProfileOptional = userProfileRepository.findById(userProfile.getId());

        if (!userProfileOptional.isPresent())
            return ResponseEntity.notFound().build();

        userProfile.setId(userProfileOptional.get().getId());

        userProfileRepository.save(userProfile);

        return ResponseEntity.noContent().build();
    }

    @Override
    public void deleteUserProfile(final Long userProfileId) {
        userProfileRepository.deleteById(userProfileId);
    }
}
