package com.task.demo.service;

import com.task.demo.model.UserProfile;
import org.springframework.http.ResponseEntity;

/**
 * Created by hassanadeel on 18/6/18.
 */
public interface IUserProfileService {
    UserProfile createUserProfile(final UserProfile userProfile);

    ResponseEntity updateUserProfile(final UserProfile userProfile);

    void deleteUserProfile(final Long userProfileId);
}
