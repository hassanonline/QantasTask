package com.task.demo.repository;

import com.task.demo.model.UserProfile;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by hassanadeel on 18/6/18.
 */
public interface IUserProfileRepository extends CrudRepository<UserProfile, Long> {
    UserProfile findById(final long id);
}
