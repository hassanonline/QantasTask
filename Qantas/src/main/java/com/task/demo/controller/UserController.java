package com.task.demo.controller;

import com.task.demo.model.UserProfile;
import com.task.demo.service.UserProfileServiceImp;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by hassanadeel on 18/6/18.
 * This class is used to intercept all http calls intended for User Profile
 */
@RestController
@RequestMapping("/")
public class UserController {

    @Autowired
    private UserProfileServiceImp userProfileServiceImp;

    @RequestMapping(path = "/createUser", method = RequestMethod.POST)
    @ApiOperation("Create a User from the system.")
    public void listUser(@ApiParam("User information for a new user to be created.") @RequestBody UserProfile userProfile) {
        userProfileServiceImp.createUserProfile(userProfile);
    }

    @RequestMapping(path = "/updateUser", method = RequestMethod.POST)
    @ApiOperation("Update a User from the system.")
    public void updateUser(@ApiParam("User information for a user to be updated.") @RequestBody UserProfile userProfile) {
        userProfileServiceImp.createUserProfile(userProfile);
    }

    @ApiOperation("Deletes a person from the system. 404 if the person's identifier is not found.")
    @RequestMapping(path = "/deleteUser/{userProfileId}", method = RequestMethod.DELETE)
    public void updateUser(@ApiParam("Id of the user to be deleted. Cannot be empty.") @PathVariable Long userProfileId) {
        userProfileServiceImp.deleteUserProfile(userProfileId);
    }
}
